#!/usr/bin/env python3
import json
import subprocess
import time
init_file = "init.json"


class app(object):
    shell = ""
    runlist = list()
    processes = list()
    retunrncode = 0
    endflag = False

    def parse(self, init_file):
        with open(init_file, "r") as read_file:
            data = json.load(read_file)
            self.runlist = data["runlist"]
            self.shell = data["shell"]

    def run(self):
        for task in self.runlist:
            p = subprocess.Popen([self.shell, '-c', task])
            self.processes.append(p)

    def watch(self):
        while not self.endflag:
            time.sleep(1)
            for p in self.processes:
                if p.poll() != None:
                    self.endflag = True
                    self.processes.remove(p)
                    self.set_returncode(p.poll())

    def set_returncode(self, r):
        if r != 0:
            self.retunrncode = 1


def main():
    print("Started")
    App = app()
    App.parse(init_file)
    App.run()
    App.watch()
    print("Ended")
    exit(App.retunrncode)


main()
